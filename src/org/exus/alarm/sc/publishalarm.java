package org.exus.alarm.sc;

import com.indra.sofia2.ssap.kp.implementations.rest.SSAPResourceAPI;
import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import com.indra.sofia2.ssap.kp.implementations.rest.resource.SSAPResource;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import org.exus.timestamp.timestamp;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class publishalarm {
//185.119.248.15:8080/sib/...
    String szURL = "http://192.168.0.208:8080/sib/services/api_ssap/";
    SSAPResourceAPI p = null;
    String sessionkey;
    SSAPResource pres = new SSAPResource();

    public static void main(String[] args) throws ResponseMapperException, IOException {
publishalarm pp = new publishalarm();
pp.Connect();
pp.NowInsert();
//pp.close();
    }
    
    public void Connect() throws ResponseMapperException {
        if (p == null) {
            p = new SSAPResourceAPI(szURL);
        }

        pres.setJoin(true);

        pres.setToken("77bcfc0c6ddb4c92bc9f3d463cea27e6");
        pres.setInstanceKP("SocialKPOnly:SocialKPOnlyIns");

        Response presponse = p.insert(pres);
        if (presponse.getStatus() == 200) {
            //good
            sessionkey = p.responseAsSsap(presponse).getSessionKey();
//            System.out.print("Session Key : ");
            System.out.println(sessionkey);
//            System.out.println("Key got !");
        } else {
            System.out.println("error : " + presponse.getStatus());
        }
        pres.setLeave(true);

    }

    public void Disconnect() {
        pres.setLeave(true);
    }

    public void NowInsert() {

        String sidentifier = "socialmedia";
        timestamp pps = new timestamp();

        String sziii = "\"AgentSocialMedia\":{ \"identifier\":\"" + sidentifier + "\",\"timestamp\":\"" + pps.GetTimeStamp() + "\",\"stext\":\"" + "Threshold" + "\",\"alert\":\"GENERAL_OVER_THRESHOLD\"}";
        SSAPResource presl = new SSAPResource();
        presl.setSessionKey(sessionkey);
        presl.setOntology("eVACUATE_SocialMedia_onto4_v0");
        presl.setData(sziii);
        Response presponse = p.insert(presl);
        System.out.println("Insert Status Threshold Alarm : " + presponse.getStatus());
    }

    public void NowInsertFire(String ss) {

        String sidentifier = "socialmedia";
        timestamp pps = new timestamp();

        String sziii = "\"AgentSocialMedia\":{ \"identifier\":\"" + sidentifier + "\",\"timestamp\":\"" + pps.GetTimeStamp() + "\",\"stext\":\"" + ss + "\",\"alert\":\"FIRE\"}";
        SSAPResource presl = new SSAPResource();
        presl.setSessionKey(sessionkey);
        presl.setOntology("eVACUATE_SocialMedia_onto4_v0");
        presl.setData(sziii);
        Response presponse = p.insert(presl);
        System.out.println("Insert Status Fire Alarm : " + presponse.getStatus());
    }

    public void NowInsertCrowding(String ss) {

        String sidentifier = "socialmedia";
        timestamp pps = new timestamp();

        String sziii = "\"AgentSocialMedia\":{ \"identifier\":\"" + sidentifier + "\",\"timestamp\":\"" + pps.GetTimeStamp() + "\",\"stext\":\"" + ss + "\",\"alert\":\"CROWDING\"}";
        SSAPResource presl = new SSAPResource();
        presl.setSessionKey(sessionkey);
        presl.setOntology("eVACUATE_SocialMedia_onto4_v0");
        presl.setData(sziii);
        Response presponse = p.insert(presl);
        System.out.println("Insert Status Crowding Alarm : " + presponse.getStatus());
    }

    public void NowInsertExplosion(String ss) {

        String sidentifier = "socialmedia";
        timestamp pps = new timestamp();

        String sziii = "\"AgentSocialMedia\":{ \"identifier\":\"" + sidentifier + "\",\"timestamp\":\"" + pps.GetTimeStamp() + "\",\"stext\":\"" + ss + "\",\"alert\":\"EXPLOSION\"}";
        SSAPResource presl = new SSAPResource();
        presl.setSessionKey(sessionkey);
        presl.setOntology("eVACUATE_SocialMedia_onto4_v0");
        presl.setData(sziii);
        Response presponse = p.insert(presl);
        System.out.println("Insert Status Explosion Alarm : " + presponse.getStatus());
    }

    public void NowInsertFallenPerson(String ss) {

        String sidentifier = "socialmedia";
        timestamp pps = new timestamp();

        String sziii = "\"AgentSocialMedia\":{ \"identifier\":\"" + sidentifier + "\",\"timestamp\":\"" + pps.GetTimeStamp() + "\",\"stext\":\"" + ss + "\",\"alert\":\"FALLEN_PERSON\"}";
        SSAPResource presl = new SSAPResource();
        presl.setSessionKey(sessionkey);
        presl.setOntology("eVACUATE_SocialMedia_onto4_v0");
        presl.setData(sziii);
        Response presponse = p.insert(presl);
        System.out.println("Insert Status Fallen Person Alarm : " + presponse.getStatus());
    }

    public void NowInsertTransportation(String ss) {

        String sidentifier = "socialmedia";
        timestamp pps = new timestamp();


        String sziii = "\"AgentSocialMedia\":{ \"identifier\":\"" + sidentifier + "\",\"timestamp\":\"" + pps.GetTimeStamp()+ "\",\"stext\":\"" + ss +"\",\"alert\":\"TRANSPORTATIONS_HALTED\"}";
        SSAPResource presl = new SSAPResource();
        presl.setSessionKey(sessionkey);
        presl.setOntology("eVACUATE_SocialMedia_onto4_v0");
        presl.setData(sziii);

        Response presponse = p.insert(presl);

        System.out.println("Insert Status Transport Halted Alarm : " + presponse.getStatus());
    }

    //Query Example
//    //eVACUATE_WP8CountByCategory 
//    public void QueryOnto() {
//        try {
//            //        p.query(szURL, sessionkey, szURL);
////    Response presponse=p.query(szURL, sessionkey, "eVACUATE_Alarm_onto4");
////Response presponse=p.query(sessionkey, "eVACUATE_Alarm_onto4", "select * from eVACUATE_Alarm_onto4", "", "SQLLIKE");
//            Response presponse = p.query(sessionkey, "eVACUATE_SocialMedia_onto4_v0", "select * from eVACUATE_SocialMedia_onto4_v0", "", "SQLLIKE");
//
//            System.out.println("Query Status : " + presponse.getStatus());
//            System.out.println(p.responseAsSsap(presponse).getData());
//        } catch (ResponseMapperException ex) {
////            Logger.getLogger(publishalarm1.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
}
