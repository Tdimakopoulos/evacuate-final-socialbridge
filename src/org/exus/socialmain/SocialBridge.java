package org.exus.socialmain;

import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import com.ning.http.client.AsyncHttpClient;
import org.exus.alarm.sc.publishalarm;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import org.exus.df.ExusDF;
import org.exus.df.SocialChecker;
import org.exus.social.manage.readwrite;
import org.exus.social.model.socialmodel;
import org.exus.social.socialcomm;
import org.exus.timestamp.timestamp;
import org.exus.utils.utils;
import org.exus.workflow.workflow;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class SocialBridge {
//
//    public static void main(String[] args) throws InterruptedException {
//        System.out.println("Starting");
//
//        publishalarm pa = new publishalarm();
//        try {
//            
//            pa.Connect();
//        } catch (ResponseMapperException ex) {
//            System.out.println("Error : " + ex.getMessage());
//        }
//        pa.NowInsertFire("Fire Alarm");
//        pa.Disconnect();
//    }
    public static void main(String[] args) throws InterruptedException {
        
        System.out.println("Social Alarm Version : Final ");
        workflow pwork = new workflow();
        SocialChecker pcheck = new SocialChecker();

        pwork.InitialRun();
        pcheck.CheckThread(1);
        pcheck.CheckAllAlarms(-1);
        while (true) {

            int ifound = pwork.LoopRun();
            System.out.println("New Tweets : " + ifound);
            if (ifound > 0) {
                pcheck.CheckThread(0);
                pcheck.CheckAllAlarms(ifound);
            }

            Thread.sleep(90 * 1000);

        }
    }
}
