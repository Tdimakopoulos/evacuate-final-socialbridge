package org.exus.social;

import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import net.sf.json.JSONArray;
import org.exus.alarm.sc.publishalarm;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class socialcomm {

    String ServiceURL="http://vineyard-ws.telesto.gr/demosvc/jersey/jsonservice/demoalerts";
//    String ServiceTextURL="http://vineyard-ws.telesto.gr/demosvc/jersey/jsonservice/demotext";
     String ServiceTextURL="http://vineyard-ws.telesto.gr/SonemaEvacuate/jersey/jsonservice/demotext";
    
     
    public static void main(String[] args) throws ResponseMapperException {
        System.out.println("Social Alarm Detected !");
                    publishalarm pa = new publishalarm();
                    System.out.println("Sending Alarm !");
                    pa.Connect();
                    pa.NowInsert();
                    pa.Disconnect();
//     socialcomm sc=new socialcomm();
////        System.out.println(sc.GetSocialText());
//        String sYourJsonString=sc.GetSocialText();
//    Object[] arrayReceipients = (Object[]) JSONArray.toArray (JSONArray.fromObject(sYourJsonString));
//System.out.println (arrayReceipients [0]);
//System.out.println (arrayReceipients.length);
             
    }
    
//    Object[] arrayReceipients = JSONArray.toArray (JSONArray.fromObject(sYourJsonString));
//System.out.println (arrayReceipients [0]);
    
    public String GetSocialText() {

        String szreturnvalues = null;

        try {

            URL url = new URL(ServiceTextURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
//            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
//                System.out.println(output);
                szreturnvalues = output;
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();
            szreturnvalues = "Error 001";
        } catch (IOException e) {

            e.printStackTrace();
            szreturnvalues = "Error 002";
        }
        return szreturnvalues;
    }
    
    public String GetSocialValues() {

        String szreturnvalues = null;

        try {

            URL url = new URL(ServiceURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
//            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
//                System.out.println(output);
                szreturnvalues = output;
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();
            szreturnvalues = "Error 001";
        } catch (IOException e) {

            e.printStackTrace();
            szreturnvalues = "Error 002";
        }
        return szreturnvalues;
    }
}
