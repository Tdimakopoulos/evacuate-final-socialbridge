/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.social.model;

import java.io.Serializable;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class socialmodel implements Serializable  {
    private String szsocialtext;
    private String szAlertText;
    private String fire;
    private String fallenperson;
    private String explosion;
    private String transport;
    private String crow;
    
    /**
     * @return the szsocialtext
     */
    public String getSzsocialtext() {
        return szsocialtext;
    }

    /**
     * @param szsocialtext the szsocialtext to set
     */
    public void setSzsocialtext(String szsocialtext) {
        this.szsocialtext = szsocialtext;
    }

    /**
     * @return the szAlertText
     */
    public String getSzAlertText() {
        return szAlertText;
    }

    /**
     * @param szAlertText the szAlertText to set
     */
    public void setSzAlertText(String szAlertText) {
        this.szAlertText = szAlertText;
    }

    /**
     * @return the fire
     */
    public String getFire() {
        return fire;
    }

    /**
     * @param fire the fire to set
     */
    public void setFire(String fire) {
        this.fire = fire;
    }

    /**
     * @return the fallenperson
     */
    public String getFallenperson() {
        return fallenperson;
    }

    /**
     * @param fallenperson the fallenperson to set
     */
    public void setFallenperson(String fallenperson) {
        this.fallenperson = fallenperson;
    }

    /**
     * @return the explosion
     */
    public String getExplosion() {
        return explosion;
    }

    /**
     * @param explosion the explosion to set
     */
    public void setExplosion(String explosion) {
        this.explosion = explosion;
    }

    /**
     * @return the transport
     */
    public String getTransport() {
        return transport;
    }

    /**
     * @param transport the transport to set
     */
    public void setTransport(String transport) {
        this.transport = transport;
    }

    /**
     * @return the crow
     */
    public String getCrow() {
        return crow;
    }

    /**
     * @param crow the crow to set
     */
    public void setCrow(String crow) {
        this.crow = crow;
    }
    
    
    
}
