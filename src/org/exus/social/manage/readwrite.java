/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.social.manage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.exus.social.model.socialmodel;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class readwrite {

    String path="c:\\evacdata\\";
    String filedata=path+"data.dat";
    private List<socialmodel> mediatweets=new ArrayList();
    
    public void SaveList(List<socialmodel> ss) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filedata);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(ss);
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(readwrite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(readwrite.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(readwrite.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void ReadFile()
    {
        FileInputStream fis = null;
        mediatweets.clear();
        try {
            fis = new FileInputStream(filedata);
            ObjectInputStream ois = new ObjectInputStream(fis);
            mediatweets = (List<socialmodel>) ois.readObject();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(readwrite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(readwrite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(readwrite.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(readwrite.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * @return the mediatweets
     */
    public List<socialmodel> getMediatweets() {
        return mediatweets;
    }

    /**
     * @param mediatweets the mediatweets to set
     */
    public void setMediatweets(List<socialmodel> mediatweets) {
        this.mediatweets = mediatweets;
    }
}
