/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.df;

import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import org.exus.alarm.sc.publishalarm;
import org.exus.social.manage.readwrite;
import org.exus.social.model.socialmodel;
import org.exus.social.socialcomm;
import org.exus.socialmain.SocialBridge;
import org.exus.utils.utils;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class SocialChecker {

    boolean bpub = false;
    int bp = 0;

    public void CheckAllAlarms(int i) {
        try {
            readwrite pp = new readwrite();
            List<socialmodel> pTweets = new ArrayList();
            publishalarm psofia = new publishalarm();
            psofia.Connect();

            pTweets.clear();
            pp.ReadFile();
            pTweets = pp.getMediatweets();
            if (i == -1) {
                System.out.println("Initial Run check for Alarms");
                for (int ix = 0; ix < pTweets.size(); ix++) {
                    socialmodel pitem = pTweets.get(ix);
                    if (pitem.getCrow().equalsIgnoreCase("-1")) {
                    } else {
//                        psofia.NowInsertCrowding(pitem.getSzsocialtext());
                    }

                    if (pitem.getExplosion().equalsIgnoreCase("-1")) {
                    } else {
//                        psofia.NowInsertExplosion(pitem.getSzsocialtext());
                    }

                    if (pitem.getFallenperson().equalsIgnoreCase("-1")) {
                    } else {
//                        psofia.NowInsertFallenPerson(pitem.getSzsocialtext());
                    }

                    if (pitem.getFire().equalsIgnoreCase("-1")) {
                    } else {
//                        psofia.NowInsertFire(pitem.getSzsocialtext());
                    }

                    if (pitem.getTransport().equalsIgnoreCase("-1")) {
                    } else {
//                        psofia.NowInsertTransportation(pitem.getSzsocialtext());
                    }

                }
            } else if (i == 0) {
                System.out.println("Nothing to check , No new Tweets!");
            } else {
                System.out.println("New Tweets Detected, Check for Alarms");
                for (int ix = pTweets.size()-i; ix < pTweets.size(); ix++) {
                    socialmodel pitem = pTweets.get(ix);
                    if (pitem.getCrow().equalsIgnoreCase("-1")) {
                    } else {
                        psofia.NowInsertCrowding(pitem.getSzsocialtext());
                    }

                    if (pitem.getExplosion().equalsIgnoreCase("-1")) {
                    } else {
                        psofia.NowInsertExplosion(pitem.getSzsocialtext());
                    }

                    if (pitem.getFallenperson().equalsIgnoreCase("-1")) {
                    } else {
                        psofia.NowInsertFallenPerson(pitem.getSzsocialtext());
                    }

                    if (pitem.getFire().equalsIgnoreCase("-1")) {
                    } else {
                        psofia.NowInsertFire(pitem.getSzsocialtext());
                    }

                    if (pitem.getTransport().equalsIgnoreCase("-1")) {
                    } else {
                        psofia.NowInsertTransportation(pitem.getSzsocialtext());
                    }

                }

            }
        psofia.Disconnect();
        } catch (ResponseMapperException ex) {
            Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void CheckSpecial(String ss) {
        socialcomm pp = new socialcomm();
        String socialtext = pp.GetSocialText();
        ExusDF pdf = new ExusDF();
        String s1 = pdf.CheckCrow(socialtext);
        if (s1.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Crowding !");
                publishalarm pa = new publishalarm();
//                System.out.println("Sending Alarm !");
                pa.Connect();
                pa.NowInsertCrowding(ss);
                pa.Disconnect();
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String s2 = pdf.CheckExplosion(socialtext);
        if (s2.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Explosion !");
                publishalarm pa = new publishalarm();
//                System.out.println("Sending Alarm !");
                pa.Connect();
                pa.NowInsertExplosion(ss);
                pa.Disconnect();
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String s3 = pdf.CheckFallenPerson(socialtext);
        if (s3.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Fallen Persion !");
                publishalarm pa = new publishalarm();
//                System.out.println("Sending Alarm !");
                pa.Connect();
                pa.NowInsertFallenPerson(ss);
                pa.Disconnect();
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String s4 = pdf.CheckFire(socialtext);
        if (s4.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Fire !");
                publishalarm pa = new publishalarm();
//                System.out.println("Sending Alarm !");
                pa.Connect();
                pa.NowInsertFire(ss);
                pa.Disconnect();
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String s5 = pdf.CheckTransport(socialtext);
        if (s5.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Transportation Halted !");
                publishalarm pa = new publishalarm();
//                System.out.println("Sending Alarm !");
                pa.Connect();
                pa.NowInsertTransportation(ss);
                pa.Disconnect();
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void CheckThread(int ido) {

        try {
            socialcomm pp = new socialcomm();
            String svalue = pp.GetSocialValues();
            utils pu = new utils();
            String i = pu.GetValues(svalue);

            ExusDF pdf = new ExusDF();
            boolean bs = pdf.CalculateTH(i);
            if (bs == true) {
                if (bpub == false) {
                    System.out.println("Social Alarm Detected !");
                    if(ido==0)
                    {
                    publishalarm pa = new publishalarm();
//                    System.out.println("Sending Alarm !");
                    pa.Connect();
                    pa.NowInsert();
                    pa.Disconnect();
                    }
                    bpub = true;
                    bp = Integer.valueOf(i);
                } else {
                    int ix = Integer.valueOf(i);
                    if (bp == ix) {

                    } else {
                        System.out.println("Social Alarm Detected !");
                     if(ido==0)
                     {
                        publishalarm pa = new publishalarm();
//                        System.out.println("Sending Alarm !");
                        pa.Connect();
                        pa.NowInsert();
                        pa.Disconnect();
                     }
                        bpub = true;
                        bp = Integer.valueOf(i);
                    }
                }
            }
//                Thread.sleep(20000);
        } catch (ResponseMapperException ex) {
            Logger.getLogger(SocialBridge.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
