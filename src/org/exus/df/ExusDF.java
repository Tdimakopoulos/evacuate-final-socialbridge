package org.exus.df;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class ExusDF {

    public int GetValueTH() {

        FileInputStream fis = null;
        try {
            fis = new FileInputStream("c:\\evac-d\\set.soc");
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
//            while ((
                    line = br.readLine();
//                    ) != null) {
//                System.out.println(line);
//            }
            br.close();
//            System.out.println("--->"+line);
           
            return Integer.valueOf(line);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

    public String CheckTransport(String szss) {
        String sret = "-1";
        String stest = szss.toLowerCase();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("c:\\evac-d\\tran.txt");
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
            if (stest.contains(line)) {
                    sret = "TRANSPORTATIONS_HALTED";
                }
            }
            br.close();
            return sret;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "-1";

//        String sret = "-1";
//
//        String stest = szss.toLowerCase();
//        if (stest.contains("fall")) {
//            sret = "TRANSPORTATIONS_HALTED";
//        }
//        if (stest.contains("stairs")) {
//            sret = "TRANSPORTATIONS_HALTED";
//        }
//        if (stest.contains("stair")) {
//            sret = "TRANSPORTATIONS_HALTED";
//        }
//        if (stest.contains("fallen")) {
//            sret = "TRANSPORTATIONS_HALTED";
//        }
//        if (stest.contains("blood")) {
//            sret = "TRANSPORTATIONS_HALTED";
//        }
//        if (stest.contains("steps")) {
//            sret = 
//        }
    }

    public String CheckFallenPerson(String szss) {
String sret = "-1";
        String stest = szss.toLowerCase();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("c:\\evac-d\\fall.txt");
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
            if (stest.contains(line)) {
                    sret = "FALLEN_PERSON";
                }
            }
            br.close();
            return sret;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "-1";

//        String sret = "-1";
//
//        String stest = szss.toLowerCase();
//        if (stest.contains("fall")) {
//            sret = "FALLEN_PERSON";
//        }
//        if (stest.contains("stairs")) {
//            sret = "FALLEN_PERSON";
//        }
//        if (stest.contains("stair")) {
//            sret = "FALLEN_PERSON";
//        }
//        if (stest.contains("fallen")) {
//            sret = "FALLEN_PERSON";
//        }
//        if (stest.contains("blood")) {
//            sret = "FALLEN_PERSON";
//        }
//        if (stest.contains("steps")) {
//            sret = "FALLEN_PERSON";
//        }
//
//        return sret;
    }

    public String CheckExplosion(String szss) {
String sret = "-1";
        String stest = szss.toLowerCase();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("c:\\evac-d\\explosions.txt");
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
            if (stest.contains(line)) {
                    sret = "EXPLOSION";
                }
            }
            br.close();
            return sret;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "-1";

//        String sret = "-1";
//
//        String stest = szss.toLowerCase();
//        if (stest.contains("explode")) {
//            sret = "EXPLOSION";
//        }
//        if (stest.contains("explosion")) {
//            sret = "EXPLOSION";
//        }
//        if (stest.contains("bomb")) {
//            sret = "EXPLOSION";
//        }
//        if (stest.contains("loud noise")) {
//            sret = "EXPLOSION";
//        }
//        if (stest.contains("bombs")) {
//            sret = "EXPLOSION";
//        }
//        if (stest.contains("explosions")) {
//            sret = "EXPLOSION";
//        }
//
//        return sret;
    }

    public String CheckCrow(String szss) {
        String sret = "-1";
        String stest = szss.toLowerCase();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("c:\\evac-d\\crow.txt");
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
            if (stest.contains(line)) {
                    sret = "CROWDING";
                }
            }
            br.close();
            return sret;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "-1";
//        String sret = "-1";
//
//        String stest = szss.toLowerCase();
//        if (stest.contains("crowd")) {
//            sret = "CROWDING";
//        }
//        if (stest.contains("stop")) {
//            sret = "CROWDING";
//        }
//        if (stest.contains("crowding")) {
//            sret = "CROWDING";
//        }
//        if (stest.contains("no space")) {
//            sret = "CROWDING";
//        }
//        if (stest.contains("no move")) {
//            sret = "CROWDING";
//        }
//        if (stest.contains("lots of people")) {
//            sret = "CROWDING";
//        }
//
//        return sret;
    }

    public boolean CalculateTH(String value) {
        boolean bret = false;

        int ivalue = Integer.parseInt(value);

        if (ivalue == GetValueTH()) {
            bret = true;
        }
        if (ivalue > GetValueTH()) {
            bret = true;
        }

        return bret;
    }

    public String CheckFire(String szss) {
        String sret = "-1";
        String stest = szss.toLowerCase();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("c:\\evac-d\\fire.txt");
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
            if (stest.contains(line)) {
                    sret = "FIRE";
                }
            }
            br.close();
            return sret;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ExusDF.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "-1";
//        String sret = "-1";
//
//        String stest = szss.toLowerCase();
//        if (stest.contains("fire")) {
//            sret = "FIRE";
//        }
//        if (stest.contains("smoke")) {
//            sret = "FIRE";
//        }
//        if (stest.contains("flame")) {
//            sret = "FIRE";
//        }
//        if (stest.contains("flames")) {
//            sret = "FIRE";
//        }
//        return sret;
    }

}
