/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.workflow;

import java.util.ArrayList;
import java.util.List;
import net.sf.json.JSONArray;
import org.exus.df.ExusDF;
import org.exus.social.manage.readwrite;
import org.exus.social.model.socialmodel;
import org.exus.social.socialcomm;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class workflow {

    public void InitialRun() {
        socialcomm sc = new socialcomm();
        ExusDF pdf = new ExusDF();
        List<socialmodel> pTweets = new ArrayList();
        String sYourJsonString = sc.GetSocialText();
        Object[] arrayReceipients = (Object[]) JSONArray.toArray(JSONArray.fromObject(sYourJsonString));

        pTweets.clear();
        for (int i = 0; i < arrayReceipients.length; i++) {

            socialmodel pitem = new socialmodel();
            pitem.setSzsocialtext((String) arrayReceipients[i]);

            String scrow = pdf.CheckCrow((String) arrayReceipients[i]);
            String explo = pdf.CheckExplosion((String) arrayReceipients[i]);
            String person = pdf.CheckFallenPerson((String) arrayReceipients[i]);
            String fire = pdf.CheckFire((String) arrayReceipients[i]);
            String transport = pdf.CheckTransport((String) arrayReceipients[i]);

            pitem.setSzAlertText("0");
            pitem.setFire("-1");
            if (scrow.equalsIgnoreCase("-1")) {
                pitem.setCrow("-1");
            } else {
                pitem.setCrow(scrow);
                pitem.setSzAlertText((String) arrayReceipients[i]);
            }

            if (explo.equalsIgnoreCase("-1")) {
                pitem.setExplosion("-1");
            } else {
                pitem.setExplosion(explo);
                pitem.setSzAlertText((String) arrayReceipients[i]);
            }

            if (person.equalsIgnoreCase("-1")) {
                pitem.setFallenperson("-1");
            } else {
                pitem.setFallenperson(person);
                pitem.setSzAlertText((String) arrayReceipients[i]);
            }

            if (fire.equalsIgnoreCase("-1")) {
                pitem.setFire("-1");
            } else {
                pitem.setFire(fire);
                pitem.setSzAlertText((String) arrayReceipients[i]);
            }

            if (transport.equalsIgnoreCase("-1")) {
                pitem.setTransport("-1");

            } else {
                pitem.setTransport(transport);
                pitem.setSzAlertText((String) arrayReceipients[i]);
            }

            String snew = pitem.getSzAlertText().replace("#fp7evac", "");
            pitem.setSzAlertText(snew);

            pTweets.add(pitem);

//            System.out.println("-----------------------------------");
//            System.out.println(pitem.getSzsocialtext());
//
//            System.out.println(pitem.getFire());
//            System.out.println(pitem.getCrow());
//            System.out.println(pitem.getExplosion());
//            System.out.println(pitem.getFallenperson());
//            System.out.println(pitem.getTransport());
//
//            System.out.println(pitem.getSzAlertText());
        }
        readwrite pp = new readwrite();
        pp.SaveList(pTweets);
    }

    public int LoopRun() {
        readwrite pp = new readwrite();
        socialcomm sc = new socialcomm();
        ExusDF pdf = new ExusDF();
        List<socialmodel> pTweets = new ArrayList();
        String sYourJsonString = sc.GetSocialText();
        Object[] arrayReceipients = (Object[]) JSONArray.toArray(JSONArray.fromObject(sYourJsonString));

        pTweets.clear();
        pp.ReadFile();
        List<socialmodel> pTweetsold = pp.getMediatweets();
        if (pTweetsold.size() == arrayReceipients.length) {
            return 0;
        } else {
            for (int i = 0; i < arrayReceipients.length; i++) {

                socialmodel pitem = new socialmodel();
                pitem.setSzsocialtext((String) arrayReceipients[i]);

                String scrow = pdf.CheckCrow((String) arrayReceipients[i]);
                String explo = pdf.CheckExplosion((String) arrayReceipients[i]);
                String person = pdf.CheckFallenPerson((String) arrayReceipients[i]);
                String fire = pdf.CheckFire((String) arrayReceipients[i]);
                String transport = pdf.CheckTransport((String) arrayReceipients[i]);

                pitem.setSzAlertText("0");
                pitem.setFire("-1");
                if (scrow.equalsIgnoreCase("-1")) {
                    pitem.setCrow("-1");
                } else {
                    pitem.setCrow(scrow);
                    pitem.setSzAlertText((String) arrayReceipients[i]);
                }

                if (explo.equalsIgnoreCase("-1")) {
                    pitem.setExplosion("-1");
                } else {
                    pitem.setExplosion(explo);
                    pitem.setSzAlertText((String) arrayReceipients[i]);
                }

                if (person.equalsIgnoreCase("-1")) {
                    pitem.setFallenperson("-1");
                } else {
                    pitem.setFallenperson(person);
                    pitem.setSzAlertText((String) arrayReceipients[i]);
                }

                if (fire.equalsIgnoreCase("-1")) {
                    pitem.setFire("-1");
                } else {
                    pitem.setFire(fire);
                    pitem.setSzAlertText((String) arrayReceipients[i]);
                }

                if (transport.equalsIgnoreCase("-1")) {
                    pitem.setTransport("-1");

                } else {
                    pitem.setTransport(transport);
                    pitem.setSzAlertText((String) arrayReceipients[i]);
                }

                String snew = pitem.getSzAlertText().replace("#fp7evac", "");
                pitem.setSzAlertText(snew);

                pTweets.add(pitem);

//                System.out.println("-----------------------------------");
//                System.out.println(pitem.getSzsocialtext());
//
//                System.out.println(pitem.getFire());
//                System.out.println(pitem.getCrow());
//                System.out.println(pitem.getExplosion());
//                System.out.println(pitem.getFallenperson());
//                System.out.println(pitem.getTransport());
//
//                System.out.println(pitem.getSzAlertText());
            }

            pp.SaveList(pTweets);
        }
        return arrayReceipients.length-pTweetsold.size();
    }
}
